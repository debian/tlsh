Source: tlsh
Maintainer: Jérémy Bobbio <lunar@debian.org>
Section: admin
Priority: optional
Build-Depends:
 cmake,
 debhelper-compat (= 13),
 dh-python,
 libpython3-all-dev,
 python3-all-dev:any,
 python3-docutils,
 python3-setuptools,
Rules-Requires-Root: no
Standards-Version: 4.6.0
Homepage: https://github.com/trendmicro/tlsh
Vcs-Git: https://salsa.debian.org/debian/tlsh.git
Vcs-Browser: https://salsa.debian.org/debian/tlsh

Package: tlsh-tools
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: compare similar files using fuzzy hashing
 The Trend Micro Locality Sensitive Hash is a fuzzy hash algorithm that can be
 used to compare similar but not identical files.
 .
 Identifying near duplicates and similar files is known to be useful to
 identify malware samples with similar binary file structure, variants of spam
 email, or backups with corrupted files.
 .
 This package contains the tlsh_unittest utility, a command-line tool to
 generate TLSH hash values and compare TLSH hash values to determine
 similar files.

Package: libtlsh0
Section: libs
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: fuzzy hashing library
 The Trend Micro Locality Sensitive Hash is a fuzzy hash algorithm that can be
 used to compare similar but not identical files.
 .
 Identifying near duplicates and similar files is known to be useful to
 identify malware samples with similar binary file structure, variants of spam
 email, or backups with corrupted files.
 .
 This package contains the shared library itself.

Package: libtlsh-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libtlsh0 (= ${binary:Version}),
 ${misc:Depends},
Description: fuzzy hashing library - development files
 The Trend Micro Locality Sensitive Hash is a fuzzy hash algorithm that can be
 used to compare similar but not identical files.
 .
 Identifying near duplicates and similar files is known to be useful to
 identify malware samples with similar binary file structure, variants of spam
 email, or backups with corrupted files.
 .
 This package contains the development headers and the static library.

Package: python3-tlsh
Section: python
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Description: fuzzy hashing library - Python3 module
 The Trend Micro Locality Sensitive Hash is a fuzzy hash algorithm that can be
 used to compare similar but not identical files.
 .
 Identifying near duplicates and similar files is known to be useful to
 identify malware samples with similar binary file structure, variants of spam
 email, or backups with corrupted files.
 .
 This package contains the Python3 module.
